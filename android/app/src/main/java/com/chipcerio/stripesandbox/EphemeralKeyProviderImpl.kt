package com.chipcerio.stripesandbox

import com.chipcerio.stripesandbox.ext.addTo
import com.stripe.android.EphemeralKeyProvider
import com.stripe.android.EphemeralKeyUpdateListener
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import okhttp3.FormBody
import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.Response
import java.io.IOException

class EphemeralKeyProviderImpl(
    private val client: OkHttpClient,
    private val listener: OnStatusListener?
) : EphemeralKeyProvider {
    
    private val disposables = CompositeDisposable()
    
    override fun createEphemeralKey(
        apiVersion: String,
        keyUpdateListener: EphemeralKeyUpdateListener
    ) {
        networkCall()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnNext { listener?.onSuccess(it.body?.string()) }
            .doOnComplete { listener?.onFinish() }
            .doOnError { listener?.onError(it) }
            .subscribe(
                { it.body?.string()?.let { rawJson -> keyUpdateListener.onKeyUpdate(rawJson) } },
                { keyUpdateListener.onKeyUpdateFailure(0, it.message ?: "Error on EphemeralKeyUpdate") })
            .addTo(disposables)
    }
    
    private fun networkCall(): Observable<Response> {
        val localIpAddress = "192.168.1.9"
        
        val formBody = FormBody.Builder()
            .add("api_version", "2019-12-03")
            .build()
        
        val request = Request.Builder()
            .url("http://$localIpAddress:3000/ephemeral_key")
            .post(formBody)
            .build()
        
        return Observable.create<Response> { emitter ->
            try {
                val response = client.newCall(request).execute()
                if (response.isSuccessful) {
                    emitter.onNext(response)
                    emitter.onComplete()
                } else {
                    emitter.onComplete()
                }
            } catch (e: IOException) {
                emitter.onError(e)
            } catch (e: IllegalStateException) {
                emitter.onError(e)
            }
        }
    }
    
    fun clear() {
        disposables.clear()
    }
    
    interface OnStatusListener {
        fun onSuccess(rawJson: String?)
        fun onFinish()
        fun onError(e: Throwable)
    }
}