package com.chipcerio.stripesandbox

import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import com.stripe.android.CustomerSession
import com.stripe.android.PaymentSession
import com.stripe.android.PaymentSessionConfig
import com.stripe.android.PaymentSessionData
import com.stripe.android.model.PaymentMethod
import kotlinx.android.synthetic.main.activity_welcome.*

class WelcomeActivity : AppCompatActivity(), EphemeralKeyProviderImpl.OnStatusListener {
    
    private lateinit var paymentSession: PaymentSession
    
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_welcome)
        
        val client = (application as App).getClient()
        val ephemeralKeyProvider = EphemeralKeyProviderImpl(client, this)
        CustomerSession.initCustomerSession(this, ephemeralKeyProvider, Keys.PUBLISHABLE_KEY)
        
        setupPaymentSession()
        
        next_button.setOnClickListener {
            // startActivity(Intent(this, MainActivity::class.java))
            // paymentSession.presentPaymentMethodSelection()
            if (paymentSession.paymentSessionData.paymentMethod == null) {
                Log.d(TAG, "PaymentMethod NULL")
            } else {
                Log.d(TAG, "PaymentMethod: ${paymentSession.paymentSessionData.paymentMethod?.id}")
            }
            Log.d(TAG, "payment session data: ${paymentSession.paymentSessionData}")
        }
    }
    
    private var paymentMethodId = ""
    
    private fun setupPaymentSession() {
        val config = PaymentSessionConfig.Builder()
            .setShouldShowGooglePay(true)
            .setPaymentMethodTypes(listOf(PaymentMethod.Type.Card))
            .setShippingInfoRequired(false)
            .setShippingMethodsRequired(false)
            .build()
        
        paymentSession = PaymentSession(this, config)
        
        val isPaymentSessionInitialized =
            paymentSession.init(object : PaymentSession.PaymentSessionListener {
                override fun onPaymentSessionDataChanged(data: PaymentSessionData) {
                    if (data.useGooglePay) {
                        // customer intends to pay with Google Pay
                        Log.d(TAG, "uses Google Pay")
                    } else {
                        data.paymentMethod?.let { paymentMethod ->
                            // Display information about the selected payment method
                            Log.d(TAG, "payment method: $paymentMethod")
                        }
                    }
                    
                    if (data.isPaymentReadyToCharge) {
                        // Use the data to charge payment
                        Log.d(TAG, "isPaymentReadyToCharge: true")
                    } else {
                        Log.d(TAG, "isPaymentReadyToCharge: false")
                    }
                }
                
                override fun onCommunicatingStateChanged(isCommunicating: Boolean) {
                    if (isCommunicating) {
                        // update UI to indicate that network communication is in progress
                    } else {
                        // update UI to indicate that network communication has completed
                    }
                }
                
                override fun onError(errorCode: Int, errorMessage: String) {
                }
            })
        
        if (isPaymentSessionInitialized) {
            next_button.isEnabled = true
        }
    }
    
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (data != null) {
            paymentSession.handlePaymentData(requestCode, resultCode, data)
        }
    }
    
    override fun onSuccess(rawJson: String?) {
        rawJson?.let { Log.d(TAG, it) }
    }
    
    override fun onFinish() {
        Log.d(TAG, "Finishing the stream")
    }
    
    override fun onError(e: Throwable) {
        Log.e(TAG, "Error in creating ephemeral key", e)
    }
    
    companion object {
        const val TAG = "WelcomeActivity"
    }
}