package com.chipcerio.stripesandbox

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_main.add_payment_method_button as addPaymentBtn
import kotlinx.android.synthetic.main.activity_main.purchase_button as purchaseBtn

class MainActivity : AppCompatActivity() {
    
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        
        purchaseBtn.setOnClickListener {
            startActivity(Intent(this, PurchaseActivity::class.java))
        }
        
        addPaymentBtn.setOnClickListener {
        
        }
    }
}
