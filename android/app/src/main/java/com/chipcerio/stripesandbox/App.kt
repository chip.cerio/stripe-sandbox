package com.chipcerio.stripesandbox

import android.app.Application
import com.stripe.android.PaymentConfiguration
import okhttp3.OkHttpClient

class App : Application() {
    
    private lateinit var okHttpClient: OkHttpClient
    
    override fun onCreate() {
        super.onCreate()
        okHttpClient = OkHttpClient()
        PaymentConfiguration.init(this, Keys.PUBLISHABLE_KEY)
    }
    
    fun getClient(): OkHttpClient = okHttpClient
}