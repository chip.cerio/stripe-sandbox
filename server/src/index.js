import express from 'express';
import { publishableKey, secretKey } from './keys';
import Stripe from 'stripe';

const app = express();
app.use(express.json());
app.use(express.urlencoded());

const stripe = new Stripe(secretKey);

// cus_GeuLoymKQvhunS

app.get('/', (req, res) => {
  res.status(200).send('Hello world');
});

app.post('/ephemeral_key', (req, res) => {
  // determines which user is requesting ephemeral_key

  const { api_version } = req.body

  stripe.ephemeralKeys.create(
    // Stripe customer is pre-created for the sake of example
    { customer: 'cus_GeuLoymKQvhunS' },
    { apiVersion: api_version },
  ).then(response => {
    console.log('ephemeralKeys.create:', response);
    // attach response to User
    res.status(200).send(response);
  }).catch(() => {
    console.error('Error in creating ephemeral key');
    res.status(400).send({ message: 'Error in creating ephemeral key' });
  });
});

app.listen(3000, () => {
  console.log('App listening on port 3000')
});